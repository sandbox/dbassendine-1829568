<?php

/**
 * Implements hook_menu().
 */
function echo_api_menu() {
  $items['admin/settings/echo-api'] = array(
    'title' => 'ECHO API',
    'description' => 'Administer ECHO API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('echo_api_settings'),
    'access arguments' => array('Administer ECHO API'),
    'file' => 'echo_api.admin.inc',
  );
  $items['admin/settings/echo-api/settings'] = array(
    'title' => 'Settings',
    'description' => 'Administer ECHO API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('echo_api_settings'),
    'access arguments' => array('Administer ECHO API'),
    'file' => 'echo_api.admin.inc',
  );
  $items['admin/settings/echo-api/test'] = array(
    'title' => 'ECHO test message',
    'desc' => 'Send a test message to ECHO API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('echo_api_test'),
    'access arguments' => array('Administer ECHO API'),
    'file' => 'echo_api.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function echo_api_permission() {
  return array(
    'administer echo api' => array(
      'title' => t('Administer ECHO API'),
    ),
  );
}

/**
 * Connect to ECHO API and send message
 *
 *  @param $resource
 *    Resource path to query (eg. /datasets)
 *  @param $query
 *    Query string to submit (eg. a keyword search)
 *  @param $method
 *    HTTP method to use (eg. POST, GET etc)
 *  @return
 *    Array containing:
 *      'response': original http response
 *      'data': decoded result data
 */
function echo_api_send($resource, $query, $method) {
  // Encode message
  $query_encoded = _echo_api_encode_xml($query);

  // Send message to ECHO API
  $response = echo_api_http($resource, $query_encoded, $method);

  // Return API response upstream
  return $response;
}

/**
 * Connect to ECHO API and send
 *
 *  @param $resource
 *    Resource path to query (eg. /datasets)
 *  @param $query
 *    Query to submit (eg. a keyword search)
 *  @param $method
 *    HTTP method to use (eg. POST, GET etc)
 *  @return
 *    Array containing:
 *      'response': original http response
 *      'data': decoded result data
 */
function echo_api_http($resource, $query, $method) {
  // Get the endpoint URL
  $url = variable_get('echo_api_url');

  // Submit request, return response
  $response = drupal_http_request($url . $resource, array(
    'headers' => array(
      'content-type' => 'application/xml',
      'accept' => 'application/xml',
    ),
    'method' => $method,
    'data' => $query,
  ));

  // Handle errors & decode XML
  $response = echo_api_response_handler($method, $response);

  // Return API response upstream
  return $response;
}

/**
 * Handle HTTP response: decode XML and log errors
 *
 *  @param $method
 *    ECHO API method (returned errors vary with method)
 *  @param $response
 *    HTTP response object
 *  @return
 *    Array containing:
 *      'response': original http response
 *      'data': decoded result data
 */
function echo_api_response_handler($method, $response) {
  $return = array();

  // Return original response
  $return['response'] = $response;

  // Decode response XML
  if ($response->headers['echo-hits'] > 0) {
    $return['data'] = _echo_api_decode_xml($response->data);
  }

  // Logging
  _echo_api_response_logger($method, $response);

  // Return data and response upstream
  return $return;
}

/**
 * Helper functions
 */

/**
 * Encode XML - uses SimpleXML
 */
function _echo_api_encode_xml($query) {
  $sxe = new SimpleXMLElement('<query />');
  $for = $sxe->addChild('for');
  $for->addAttribute('value', 'collections');
  $dataCenter = $sxe->addChild('dataCenterId');
  $dataCenter->addChild('all');

  $where = $sxe->addChild('where');
  $condition = $where->addChild('collectionCondition');
  $science_keyword = $condition->addChild('scienceKeywords');
  $science_keyword_type = $science_keyword->addChild('scienceKeyword');
  $science_keyword_type_sub = $science_keyword_type->addChild('anyKeyword');
  $science_keyword_type_sub->addChild('textPattern', $query);

  return $sxe->asXML();
}

/**
 * Decode XML - uses SimpleXML
 */
function _echo_api_decode_xml($xml) {
  $xml = simplexml_load_string($xml);
  $xml = (array) $xml;
  foreach ($xml['reference'] as $reference) {
    $return[] = (array) $reference;
  }
  return $return;
}

/**
 * Logging in watchdog based on HTTP response
 *
 *  @param $method
 *    External API method (returned errors vary with method)
 *  @param $response
 *    Original HTTP response object
 */
function _echo_api_response_logger($method, $response) {
  // Flags
  $success = FALSE; // received successfully
  $error = FALSE; // received but error
  $down = FALSE; // not received, servuce down

  // Received OK - HTTP 200
  // SOAP: custom error messages normally set in "data"
  // REST: erros messages returned as other HTTP codes
  if ($response->code == 200) {
    // Check status message
    $api_response = $response->status_message;
    if ($api_response == 'OK') {
      $success = $api_response;
    }
    else {
      $error = $api_response;
    }

  }

  // Error - HTTP response code != 200
  // SOAP: only for "not found" ie. ECHO API is down
  // REST: includes full range of error messages
  else {
    switch ($response->code) {
      default:
        $api_response = $response->error;
        $error = $api_response;
      break;
      case '404':
        $api_response = $response->error;
        $down = $api_response;
      break;
    }
  }

  // Log
  // Success
  if ($success) {
    if (variable_get('echo_api_log_everything', 0) == 1) {
      watchdog('echo_api', 'Message received successfully by ECHO API with response %success. Details: type: %method; full request: %request', array('%method' => $method, '%success' => $success, '%request' => $response->request), WATCHDOG_INFO);
    }
  }
  // Error
  if ($error) {
    watchdog('echo_api', 'Message not accepted by ECHO API with error code %error. Details: type: %method; full request: %request', array('%method' => $method, '%error' => $error, '%request' => $response->request), WATCHDOG_ERROR);
  }
  // Down
  if ($down) {
    watchdog('echo_api', 'External API not available, returning HTTP code %httpcode with message %httpmessage', array('%httpcode' => $response->code, '%httpmessage' => $down), WATCHDOG_ALERT);
  }
}

