<?php
/**
 * @file
 * ECHO API administration
 * 
 * Set URL of destination webservice, and private key
 */

/**
 * Define form for administration page
 */
function echo_api_settings() {
  $form = array();
  $form['echo_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#default_value' => variable_get('echo_api_url', ''),
    '#description' => t('Enter the URL of the ECHO service to use'),
  );
  $form['echo_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#default_value' => variable_get('echo_api_key', 'abc'),
    '#description' => t('Enter the URL of the private key used to verify the authenticity of outgoing messages'),
  );
  $form['echo_api_log_everything'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log all messages?'),
    '#default_value' => variable_get('echo_api_log_everything', 0),
    '#description' => t('Enable to log successful as well as failed messages. Only recommend for debugging or development, as this will create a large volume of log messages'),
  );
  return system_settings_form($form);
}

/**
 * Test by sending sample values to external service
 */
function echo_api_test($form_state) {
  $form = array();
  $form['echo_api_test_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Keyword search of datasets (POST)'),
    '#description' => t('Enter a keyword to return matching datasets from the ECHO Catalog (nb. case sensitive)'),
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Submit handler for test message  form
 */
function echo_api_test_submit($form, &$form_state) {
  // Display feedback
  if ($form_state['values']['echo_api_test_keyword']) {
    dsm(echo_api_send('datasets/search', $form_state['values']['echo_api_test_keyword'], 'POST'));
  }
}
